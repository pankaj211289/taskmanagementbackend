package com.project.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project.models.User;
import com.project.services.UserService;

@RestController
@RequestMapping("/test")
public class TestRest {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="", method= RequestMethod.POST)
	public User getSuccessMessage(){
		return userService.searchUser("abc");
	}
}
