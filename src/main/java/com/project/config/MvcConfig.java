package com.project.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
public class MvcConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addViewControllers(ViewControllerRegistry registry){
//		registry.addViewController("/login").setViewName("login");
	}
	
	@Override
	public void addCorsMappings(CorsRegistry registry){
		registry.addMapping("/**")
				.allowedOrigins("http://localhost:3000")
				.allowedMethods("PUT", "DELETE", "POST", "GET")
//				.allowedHeaders("header1", "header2", "header3", "Content-Type", "Accept", "X-Requested-With", "remember-me",
//						"Authorization", "Access-Control-Request-Headers", "Access-Control-Request-Method", "Origin")
				.allowedHeaders("*")
				.allowCredentials(false).maxAge(3600);
	}
	
//	@Bean(name = "dataSource")
//	public DriverManagerDataSource dataSource(){
//		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
//		driverManagerDataSource.setDriverClassName("com.mysql.jdbc.Driver");
//		return driverManagerDataSource;
//	}
}
