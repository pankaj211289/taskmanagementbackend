package com.project.services;

import com.project.models.User;

public interface UserService {
	
	public User searchUser(String username);
}
